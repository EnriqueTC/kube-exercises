Para arrancar el pod usaremos el siguiente comando:
	 kubectl create -f ex1-pod.yml

1-¿Cómo puedo obtener las últimas 10 líneas de la salida estándar (logs
generados por la aplicación)?

	Para ver los últimos 10 logs hemos de usar el comando: kubectl logs --tail=10 nginx-demo. (Adjunto img1.png)

2- ¿Cómo podría obtener la IP interna del pod? Aporta capturas para indicar
el proceso que seguirías.

	Para obtener la dirección interna del pod debemos usar el comando:  kubectl describe pods nginx-demo.
	La información que nos muestra dónde pone IP: indica la IP interna del pod. (Adjunto img2.png)


3- ¿Qué comando utilizarías para entrar dentro del pod?

	Para entrar dentro del pod hemos de usar el comando: kubectl exec -it nginx-demo -- sh. (Adjunto img3.png)

4- Necesitas visualizar el contenido que expone NGINX, ¿qué acciones
debes llevar a cabo?

	Para visualizar el contenido que expone NGINX usaremos el comando: kubectl port-forward nginx-demo 80:80 para exponerlo en el puerto 80.
	Una vez realizado, podemos entrar a localhost:80 para visualizar el contenido. (Adjunto img4.png y img5.png)

5- Indica la calidad de servicio (QoS) establecida en el pod que acabas de
crear. ¿Qué lo has mirado?

	La QoS del pod es Guaranteed. Para comprobar la QoS he mirado en la documentación oficail de kubernetes:
	(enlace: https://kubernetes.io/docs/tasks/configure-pod-container/quality-service-pod/)
	Como se puede observar en el enlace anterior, el pod creado en el ejercicio cumple los requisitos siguientes:
		- Tener un memory limit y una memory request, y a más a más han de tener el mismo valor, en este caso 256Mi.
		- Tener un CPU limit y una CPU request, y a más a más han de tener el mismo valor, en este caso 100 milicores.
	
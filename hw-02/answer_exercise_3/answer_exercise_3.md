Primero de todo ejecutamos el yaml del ejercicio anterior:
	
	kubectl create -f replicaset.yml

Una vez ejecutado, creamos los diferentes services usando el commando:

	kubectl create -f service1.yml
	kubectl create -f service2.yml
	kubectl create -f service3.yml

Explicación del type en cada service.

	service1: LoadBalancer -> permite exponer el servicio al exterior.
	service2: ClusterIP -> no permite el acceso del exterior.
	service3: NodePort -> abre un puerto específico para la VM.

Una vez creados usaremos el siguiente comando para comprobar que se han creado correctamente y 
que los type corresponden con las especifiaciones de cada service: 
(Adjunto img1.png)

	kubectl get svc
	
Como podemos observar ek service1 no tiene asignada una external IP, ya que nos aparece como pending.
Para ello usaremos el siguiente comando para comprobar que efectivamente se le asigna un aIP externa:
(Adjunto img2.png)

	minikube tunnel
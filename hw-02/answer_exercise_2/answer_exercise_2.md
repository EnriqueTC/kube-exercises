Para crear el objeto ReplicaSet a partir del yaml anterior, tenemos que: (Adjunto exercici2.yml)
	- modificar el kind a RemplicaSet
	- modificar la version a apps/v1
	- especificar las replicas a 3

Una vez modificado el yaml, procedemos a la creación:

	kubectl create -f exercici2.yml

Para comprobar que se ha creado correctamente se puede usar el comando:

	kubectl get rs

- ¿Cúal sería el comando que utilizarías para escalar el número de replicas a
10?

	El comando a utilizar es:  kubectl scale --replicas=10 rs/nginx-demo
	(Adjunto img1.png)

- Si necesito tener una replica en cada uno de los nodos de Kubernetes,
¿qué objeto se adaptaría mejor?

	Para tener una replica en cada nodo de Kubernete la mejor opción seria DaemonSet.
	DaemonSet garantiza una copia del Pod para cada nodo que se crea.


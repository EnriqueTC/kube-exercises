Para realizar el deployment Blue Green usaremos 4 ficheros


Primero de todo levantaremos el servicio y el objeto de tipo deployment con el comando: (Adjunto img1.png)
	kubectl apply -f blueGreen-v1.yml -f blueGreen-service-v1.yml

Una vez levantados comprobamos que se han creado correctamente. (Adjunto img2.png y img3.png)

--Hasta aquí correspondría el apartado A

El siguiente paso es crear el Green, es decir, los nuevos pods que contienen la nueva versión. 

Para ello usaremos el fichero blueGreen-v2.yml y el comando: (Adjunto img4.png)
	kubectl apply -f blueGreen-v2.yml

Ahora debemos comprobar que se han levantado los nuevos pods y como podemos observar en la imagen adjunta, hay 4, 
que equivalen a los 2 pods del primer deploy (Blue) y los 2 pods del segundo deploy (Green). (Adjunto img5.png)

De esta manera el service sigue apuntado a la versión 1 (Blue) y los usuarios se conectan a esa versión, 
y la versión 2 (Green) podríamos dar solo acceso a los clientes para que testeen las nuevas funcionalidades.

--Hasta aquí correspondría el apartado B

Ahora procederemos a cambiar dónde apunte el service, es decir, la versión 2 (Green) y de esta manera los usuarios también verán el contenido de esta última.

Para ello usaremos el comando: (Adjunto img6.png)
	kubectl apply -f blueGreen-service-v2.yml

Para comprobar realmente que el service apunta a los nuevos pods lo podemos comprobar en el dashboard de minikube.
En la imagen adjunte se puede ver los pods a los que está apuntando el service, y estos pods son los de la versión 2 (Green).
(Adjunto img7.png)

Por último nos queda matar los dos pods de la versión 1 (Blue). Para ellos usaremos el siguiente comando:
	kubectl delete deployment nginx-blue

Como se puede observar en la imagen adjunta se han borrado los pods que pertenecían al deploy de la versión 1, el Blue.
(Adjunto img8.png)


--Hasta aquí correspondría el apartado C
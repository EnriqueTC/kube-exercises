### RECREATE ###

Primero debemos crear el objeto yaml especificando el tipo Deployment, el tipo Recreate en las especificaciones y 
pondremos que tenga 3 replicas. (recreate.yaml)

Una vez hecho el fichero yaml, toca arrancar el objeto deployment y para ello usaremos el siguiente comando:
	 kubectl create -f recreate.yml
Para verificar que se ha creado correctamente usaremos el comando:
	kubectl get deployments
(Adjunto img1.png)

Ahora procedemos a desplegar una nueva versión. Para ello subiremos la versión del nginx.

Primero comprovaremos la versión de la imagen de alguno de los pods.
Como se puede observar tiene la imagen definida en el objeto yaml.
(Adjunto img2.png)

El siguiente paso es subir la versión de la imagen. (Adjunto img3.png)
También podemos usar el comando: kubectl apply -f recreate.yml si modificamos el yaml con la subida de versión.

Una vez subida la versión, esperaremos a que se creen los nuevos pods. 
Como podemos comprobar en la imagen adjunta, los pods están en estado terminating, ya que de eso trata Recreate, primero mata los pods y luego levanta los nuevos. 
(Adjunto img4.png)

Por último nos queda comprobar la versión de los nuevos pods, y como se puede comprobar en la imagen la versión ha subido.
(Adjunto img5.png)

### ROLLING UPDATE ###

Primero debemos crear el objeto yaml. Para ello cogeremos el yaml del apartado anterior (Recreate) y especificaremos el tipo RollingUpdate. (rollingUpdate.yaml)

Una vez hecho el fichero yaml, toca arrancar el objeto deployment y para ello usaremos el siguiente comando:
	 kubectl create -f rollingUpdate.yml
Para verificar que se ha creado correctamente usaremos el comando:
	kubectl get deployments
(Adjunto img6.png)

Ahora procedemos a desplegar una nueva versión. Para ello subiremos la versión del nginx.

Primero comprovaremos la versión de la imagen de alguno de los pods.
Como se puede observar tiene la imagen definida en el objeto yaml.
(Adjunto img7.png)

El siguiente paso es subir la versión de la imagen. (Adjunto img8.png)
También podemos usar el comando: kubectl apply -f recreate.yml si modificamos el yaml con la subida de versión.

Una vez subida la versión, esperaremos a que se creen los nuevos pods. 
Como podemos comprobar en la imagen adjunta, primero crea los nuevos pods (nginx-deployment-5665bdfcf5-XXX) y cuando están los 3 a running mata los otros pods y por eso se muestran en estado terminating.
Este es el funcionamiento de Rolling Update. 
(Adjunto img9.png)

Por último nos queda comprobar la versión de los nuevos pods, y como se puede comprobar en la imagen la versión ha subido.
(Adjunto img10.png)

### ROLLOUT ###

Para hacer un rollout primero vamos a buscar la última versión con el comando: (Adjunto img11.png)
	kubectl rollout history deploy nginx-deployment
Como se puede observar en la imagen hay dos versiones. Esto quiere decir que la versión 2 es la actual y la 1 la anterior.
Para comprobarlo usaremos el siguiente comando para ver el detalle de la versión 1: (Adjunto img11.png)
	kubectl rollout history deploy nginx-deployment --revision=1
Una vez comprobado realizaremos el rollout con el siguiente comando especificando la versión que queremos, en este caso la 1: (Adjunto img12.png)
	kubectl rollout undo deploy nginx-deployment --to-revision=1
Para comprobar que se ha realizado correctamente consultaremos los pods y la versión de nginx que tiene para comprobar que es la versión anterior, es decir la primera versión que ejectuamos en el deploy.

